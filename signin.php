<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="css/login.css">
</head>

<body>
    <?php
        if(isset($_SESSION['ERROR_MESSAGE']))
            echo $_SESSION['ERROR_MESSAGE'];
    ?>
    <form action="login.php" method="POST">
        <input type="text" name="username" placeholder="Username...">
        <input type="password" name="password" placeholder="Password...">
        <button type="submit">Login</button>
    </form>
</body>
</html>